﻿namespace RabbitMqDumper.MqClient
{
    using System;

    public interface IQueueClient : IDisposable
    {
        void Initialize(RabbitMqSettings settings);
        string GetMessage();
    }
}