﻿namespace RabbitMqDumper.MqClient
{
    using System.Text;
    using JetBrains.Annotations;
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;

    [UsedImplicitly]
    internal class RabbitMqClient : IQueueClient
    {
        private IConnection connection;
        private IModel channel;
        private QueueingBasicConsumer consumer;

        private RabbitMqSettings rabbitMqSettings;

        public void Initialize(RabbitMqSettings settings)
        {
            rabbitMqSettings = settings;

            var connectionFactory = new ConnectionFactory
                                        {
                                            UserName = rabbitMqSettings.UserName,
                                            Password = rabbitMqSettings.Password,
                                            Protocol = rabbitMqSettings.Protocol ?? Protocols.FromEnvironment(),
                                            Port = rabbitMqSettings.Port,
                                            HostName = rabbitMqSettings.HostName,
                                        };

            connection = connectionFactory.CreateConnection();
            channel = connection.CreateModel();
            consumer = new QueueingBasicConsumer(channel);
            channel.BasicConsume(rabbitMqSettings.QueueName, false, consumer);
        }

        public string GetMessage()
        {
            BasicDeliverEventArgs messageFromQueue;
            if (rabbitMqSettings.IsPatient)
            {
                messageFromQueue = (BasicDeliverEventArgs) consumer.Queue.Dequeue();
            }
            else
            {
                object message;
                consumer.Queue.Dequeue(1000, out message);
                messageFromQueue = (BasicDeliverEventArgs) message;
            }

            if (messageFromQueue == null) return null;

            if (rabbitMqSettings.IsDestructive)
                channel.BasicAck(messageFromQueue.DeliveryTag, false);

            return Encoding.UTF8.GetString(messageFromQueue.Body);
        }

        public void Dispose()
        {
            channel.Close();
            connection.Close();
        }
    }
}