﻿namespace RabbitMqDumper.MqClient
{
    using RabbitMQ.Client;

    public class RabbitMqSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public IProtocol Protocol { get; set; }
        public int Port { get; set; }
        public string HostName { get; set; }
        public string QueueName { get; set; }
        public bool IsDestructive { get; set; }
        public bool IsPatient { get; set; }
    }
}