﻿namespace RabbitMqDumper.Runner
{
    public interface IRunnerOptions
    {
        string HostName { get; set; }
        string QueueName { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        int Port { get; set; }
        bool IsDestructive { get; set; }
        int Count { get; set; }
        bool IsPatient { get; set; }
        bool IsSilent { get; set; }
    }
}