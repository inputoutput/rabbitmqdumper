﻿namespace RabbitMqDumper.Runner
{
    using CommandLine;
    using CommandLine.Text;
    using RabbitMQ.Client;

    public class Options : IRunnerOptions
    {
        [Option('h', "host", Required=true, HelpText="RabbitMq host to connect with.")]
        public string HostName { get; set; }

        [Option('q', "queue", Required = true, HelpText = "RabbitMq queue to get messages from.")]
        public string QueueName { get; set; }

        [Option('u', "user", Required = false, HelpText = "RabbitMq user.", DefaultValue = "guest")]
        public string UserName { get; set; }

        [Option('p', "password", Required = false, HelpText = "RabbitMq user password.", DefaultValue = "guest")]
        public string Password { get; set; }

        [Option("port", Required = false, HelpText = "RabbitMq port.", DefaultValue = AmqpTcpEndpoint.UseDefaultPort)]
        public int Port { get; set; }

        [Option('d', "destructive", Required = false, HelpText = "Destruct messages in queue.", DefaultValue = false)]
        public bool IsDestructive { get; set; }

        [Option("patient", Required = false, HelpText = "If set, dumper waits for new messages until count is exceeded.", DefaultValue = false)]
        public bool IsPatient { get; set; }

        [Option('s', "silent", Required = false, HelpText = "Silent mode - no additional output.")]
        public bool IsSilent { get; set; }

        [Option('c', "count", Required = false, HelpText = "Number of messages to get from queue.", DefaultValue = -1)]
        public int Count { get; set; }



        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }


}