﻿namespace RabbitMqDumper.Runner
{
    public interface IRunner
    {
        void Run(IRunnerOptions runnerOptions);
    }
}