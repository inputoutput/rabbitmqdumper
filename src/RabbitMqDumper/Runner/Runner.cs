﻿namespace RabbitMqDumper.Runner
{
    using System;
    using Dumper;
    using JetBrains.Annotations;
    using MqClient;

    [UsedImplicitly]
    internal class Runner : IRunner
    {
        private readonly IQueueClient queueClient;
        private readonly IDumper dumper;

        public Runner(IQueueClient queueClient, IDumper dumper)
        {
            this.queueClient = queueClient;
            this.dumper = dumper;
        }

        public void Run(IRunnerOptions runnerOptions)
        {
            try
            {
                InitializeQueueClient(runnerOptions);

                if (runnerOptions.Count < 0)
                {
                    string message;
                    while ((message = queueClient.GetMessage()) != null)
                    {
                        dumper.Dump(message);
                    }
                }
                else
                {
                    for (var i = 0; i < runnerOptions.Count; i++)
                    {
                        var message = queueClient.GetMessage();

                        if (message == null) break;

                        dumper.Dump(message);
                    }
                }
            }
            finally
            {
                queueClient.Dispose();

                if (!runnerOptions.IsSilent)
                {
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
            }
        }

        private void InitializeQueueClient(IRunnerOptions runnerOptions)
        {
            var settings = new RabbitMqSettings
                               {
                                   HostName = runnerOptions.HostName,
                                   UserName = runnerOptions.UserName,
                                   Password = runnerOptions.Password,
                                   QueueName = runnerOptions.QueueName,
                                   Port = runnerOptions.Port,
                                   IsDestructive = runnerOptions.IsDestructive,
                                   IsPatient = runnerOptions.IsPatient
                               };

            queueClient.Initialize(settings);
        }
    }
}