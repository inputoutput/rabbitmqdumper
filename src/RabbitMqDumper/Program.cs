﻿namespace RabbitMqDumper
{
    using System;
    using Dumper;
    using JetBrains.Annotations;
    using MqClient;
    using Runner;
    using SimpleInjector;

    [UsedImplicitly]
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var options = new Options();

            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                var container = RegisterDependencies();
                container.GetInstance<IRunner>().Run(options);
            }
        }

        private static Container RegisterDependencies()
        {
            var container = new Container();
            container.Register<IRunner, Runner.Runner>();
            container.Register<IQueueClient, RabbitMqClient>();
            container.Register<IDumper, ConsoleDumper>();
            container.Verify();

            return container;
        }
    }
}