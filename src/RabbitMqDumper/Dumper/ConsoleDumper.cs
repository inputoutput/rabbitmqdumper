﻿namespace RabbitMqDumper.Dumper
{
    using System;
    using JetBrains.Annotations;

    [UsedImplicitly]
    class ConsoleDumper : IDumper
    {
        public void Dump(object obj)
        {
            Console.WriteLine(obj);
        }
    }
}