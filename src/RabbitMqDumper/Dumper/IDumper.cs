﻿namespace RabbitMqDumper.Dumper
{
    internal interface IDumper
    {
        void Dump(object obj);
    }
}